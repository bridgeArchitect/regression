#include <stdlib.h>
#include <iostream>
#include <fstream>

#include "regr.h"

#define BUFSIZE 1000

using namespace std;

/* function to read long number */
void readLongNumber(char *mess, long * number) {

	/* write message and read long number*/
	cout << mess << "\n";
	cin >> *number;

}

/* function to read row */
void readRow(char *mess, char * row) {

	/* write message and read row */
	cout << mess << "\n";
	cin >> row;

}

/* function to read file and receive arrays of points*/
int readFileWithPoints(char *filename, double *x, double *y, long N) {

	/* open stream */
	fstream fs;
	fs.open(filename);

	/* values to read */
	double xCurr, yCurr;
	long counter = 0;

	/* read file */
	if (fs.is_open()) {
		while ((fs >> xCurr >> yCurr) && (counter < N)) {
			x[counter] = xCurr;
			y[counter] = yCurr;
			counter++;
		}
	}

}

/* entry point */
int main() {

	/* coefficients of regression */
	double a, b;
	/* creation array for regression*/
	double *x;
	double *y;
	/* size of array */
	long N;
	/* name of file */
	char filename[BUFSIZE];

	/* read size of array */
	readLongNumber("Number of points:", &N);
	/* read name of file */
	readRow("Name of file:", filename);

	/* create array of points */
	x = (double *) malloc(sizeof(double) * N);
	y = (double *) malloc(sizeof(double) * N);
	/* read file with points */
	readFileWithPoints(filename, x, y, N);

	/* create object of regression */
	Regression * regression = new Regression(x, y, N);

	/* make regression */
	(*regression).makeRegression(&a, &b);

	/* write answer */
	printf("%f %f\n", a, b);

	/* return default value */
	return 0;

}