#ifndef _REGR_H_
#define _REGR_H_

class Regression {

	private:
	/* arrays for regression */
	double *x;
	double *y;
	/* size of arrays */
	long N;

	public:
	/* constructor */
	Regression (double * xIn, double * yIn, long NIn);
	/* function to make regression */
	void makeRegression(double * a, double * b);

};

#endif