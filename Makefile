COMPILER := g++
CPPFLAGS := -O2 -DNDEBUG


main: main.o regr.o
	$(COMPILER) main.o regr.o -o main

main.o: main.cpp
	$(COMPILER) $(CPPFLAGS) -c main.cpp -o main.o

regr.o: regr.cpp regr.h
	$(COMPILER) $(CPPFLAGS) -c regr.cpp -o regr.o

clean:
	rm *.o & rm main
