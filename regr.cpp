#include <stdlib.h>
#include <string.h>
#include <stdio.h>

#include "regr.h"

Regression::Regression(double *xIn, double *yIn, long NIn) {

	/* copy size of arrays to object */
	N = NIn;
	/* allocate arrays */
	x = (double *) malloc(sizeof(double) * N);
	y = (double *) malloc(sizeof(double) * N);

	/* copy input arrays to object */
	memcpy(x, xIn, sizeof(double) * N);
	memcpy(y, yIn, sizeof(double) * N);

}

/* function to add elements of array */
double sumArray(double *array, long N) {

	/* initial values */
	long i = 0;
	double sum = 0.0;

	/* add elements of array */
	while (i < N) {
		sum += array[i];
		i++;
	}

	/* return answer */
	return sum;

}

/* function to add elements in 2 degree of array */
double sum2Array(double *array, long N) {

	/* initial values */
	long i = 0;
	double sum = 0.0;

	/* add elements in 2 degree of array */
	while (i < N) {
		sum += array[i] * array[i];
		i++;
	}

	/* return answer */
	return sum;

}

/* function to make scalar products of two arrays */
double scalarProd(double * array1, double * array2, long N) {

	/* initial values */
	long i = 0;
	double sum = 0.0;

	/* add components of scalar product */
	while (i < N) {
		sum += array1[i] * array2[i];
		i++;
	}

	/* return answer */
	return sum;

}

void Regression::makeRegression(double *a, double *b) {

	/* intermediate values for regression */
	double x_sum, x2_sum, y_sum, x_y_sum;

	/* calculate intermediate values */
	x_sum = sumArray(x, N);
	y_sum = sumArray(y, N);
	x2_sum = sum2Array(x, N);
	x_y_sum = scalarProd(x, y, N);

	/* calculate coefficients of regression and save result */
	*a = (N * x_y_sum - x_sum * y_sum) / (N * x2_sum - x_sum * x_sum);
	*b = (x_sum * x_y_sum - y_sum * x2_sum) / (x_sum * x_sum - N * x2_sum);

}